/* eslint-disable @typescript-eslint/no-explicit-any */
import {IndicesPutTemplateRequest, MappingProperty} from '@elastic/elasticsearch/lib/api/types';
/*
 * Copyright (C) 2019-2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {AggregationSchema} from './aggregation';

/**
 * Template output of the mapping generation
 */
export interface MappingGenTemplate {
  /**
   * All generated aggregations
   */
  aggregations: AggregationSchema;

  /**
   * All errors that occurred
   */
  errors: string[];

  /**
   * All mappings that were generated
   */
  mappings: ElasticsearchTemplateCollection;
}

export type SimpleType = MappingProperty['type'] &
  ('keyword' | 'float' | 'boolean' | 'date' | 'integer' | 'text');

/**
 * The Typemap is used to get the corresponding ElasticsearchDataType for a name provided by the ProjectReflection
 */
export interface ElasticsearchTypemap {
  /**
   * The `stringLiteral` type must always be provided
   */
  stringLiteral: {
    /**
     * The default can be chosen freely, but must be provided
     */
    default: SimpleType;
  };

  /**
   * The name of the JS type, so for `number` it would be number
   */
  [name: string]: {
    /**
     * The default ElasticsearchDataType that should be used, if no tag or only not implemented tags are found
     */
    default: SimpleType;

    /**
     * The name of the tag, so for `@integer` it would be `integer`
     */
    [name: string]: SimpleType;
  };
}

/**
 * The Fieldmap contains all tag names for fields and the corresponding fields
 *
 * The Fieldmap works in a similar fashion to the Typemap
 */
export interface ElasticsearchFieldmap {
  /**
   * The name of the tag, so for `@sortable` it would be `sortable`
   */
  [name: string]: {
    /**
     * The default value if no parameter is provided
     */
    default: {
      /**
       * To allow the usage of `prev.fields = {...prev.fields, ...fieldmap[tag.tagName].default}`
       *
       * We could also have used `default: any`, but this adds slightly more improved type-safety.
       */
      [name: string]: any;
    };

    /**
     * The tag parameters that will be ignored
     *
     * Some tag parameters might not be important for your implementation, so you can add their names here to not get
     * any errors. The `default` will be used in that case.
     */
    ignore: string[];

    /**
     * The parameters of the tag, so for `@sortable ducet` it would be `ducet`
     */
    [name: string]: {
      /**
       * To allow the usage of `prev.fields = {...prev.fields, ...fieldmap[tag.tagName][tag.text.trim()]}`
       *
       * We could also have used `default: any`, but this adds slightly more improved type-safety.
       */
      [name: string]: any;
    };
  };
}

/**
 * A collection of Elasticsearch Templates
 */
export type ElasticsearchTemplateCollection = Record<string, Omit<IndicesPutTemplateRequest, 'name'>>;
