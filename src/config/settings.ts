/*
 * Copyright (C) 2019-2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {IndicesPutTemplateRequest} from '@elastic/elasticsearch/lib/api/types';

export const settings: IndicesPutTemplateRequest['settings'] = {
  'mapping.total_fields.limit': 10_000,
  'max_result_window': 30_000,
  'number_of_replicas': 0,
  'number_of_shards': 1,
};
