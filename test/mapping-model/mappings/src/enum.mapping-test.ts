/*
 * Copyright (C) 2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ThingType} from './types';
import {MapAggTestOptions} from '../../map-agg-test-options';

/**
 * @indexable
 */
export interface Enum {
  foo: Bar;

  bar: Baz;

  type: ThingType.Enum;
}

enum Bar {
  a,
  b,
  c,
}

enum Baz {
  d = 'd',
  // eslint-disable-next-line unicorn/prevent-abbreviations
  e = 'e',
  f = 'f',
}

export const testConfig: MapAggTestOptions = {
  // Known issue: Enums only use text
  // https://gitlab.com/openstapps/core-tools/-/issues/46
  testName: 'Emums should work',
  name: ThingType.Enum,
  map: {
    maps: {
      foo: {
        type: 'text',
      },
      bar: {
        type: 'text',
      },
    },
  },
};
