/*
 * Copyright (C) 2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {PARSE_ERROR} from '../../../../lib/config/typemap';
import {ThingType} from './types';
import {MapAggTestOptions} from '../../map-agg-test-options';

/**
 * @indexable
 */
export interface DefaultGeneric {
  foo: InterfaceWithDefaultGeneric;

  type: ThingType.DefaultGeneric;
}

interface InterfaceWithDefaultGeneric<T = number> {
  bar: T;
}

export const testConfig: MapAggTestOptions = {
  testName: 'Default generics should fail',
  name: ThingType.DefaultGeneric,
  map: {
    maps: {
      foo: {
        dynamic: 'strict',
        properties: {
          bar: {
            type: PARSE_ERROR,
          },
        },
      },
    },
  },
  err: [`At "${ThingType.DefaultGeneric}::foo.bar" for Generic "T": Missing reflection, please report!`],
};
