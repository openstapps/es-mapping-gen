/*
 * Copyright (C) 2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ThingType} from './types';
import {MapAggTestOptions} from '../../map-agg-test-options';

/**
 * @indexable
 */
export interface TypeWrapperInheritance {
  /**
   * @float
   */
  numberWrapper: NumberWrapper;

  /**
   * @keyword
   */
  stringWrapper: StringWrapper;

  /**
   * @text
   */
  stringLiteralWrapper: StringLiteralWrapper;

  type: ThingType.TypeWrapperInheritance;
}

type NumberWrapper = number;
type StringWrapper = string;
type StringLiteralWrapper = 'foo';

// https://gitlab.com/openstapps/core-tools/-/merge_requests/29
export const testConfig: MapAggTestOptions = {
  testName: 'Wrapper types should inherit tags',
  name: ThingType.TypeWrapperInheritance,
  map: {
    maps: {
      foo: {
        dynamic: 'strict',
        properties: {
          numberWrapper: {
            type: 'float',
          },
          stringWrapper: {
            type: 'keyword',
          },
          stringLiteralWrapper: {
            type: 'text',
          },
        },
      },
    },
  },
};
