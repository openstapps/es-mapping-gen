/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ThingType} from './types';
import {MapAggTestOptions} from '../../map-agg-test-options';

export interface SCISO8601DateRange {
  bar: string;
}

/**
 * @indexable
 */
export interface TypeOverrides {
  foo: SCISO8601DateRange;

  type: ThingType.TypeOverrides;
}

export const testConfig: MapAggTestOptions = {
  testName: 'Premaps should support non-external types',
  name: ThingType.TypeOverrides,
  map: {
    maps: {
      foo: {
        type: 'date_range',
      },
    },
  },
};
