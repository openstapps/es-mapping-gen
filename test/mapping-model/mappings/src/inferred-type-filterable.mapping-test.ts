/*
 * Copyright (C) 2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {MapAggTestOptions} from '../../map-agg-test-options';
import {ThingType} from './types';

/**
 * @date
 */
export type SCISO8601Date = string;

/**
 * @indexable
 */
export interface InferredTypeFilterable {
  /**
   * @filterable
   */
  foo: SCISO8601Date;

  /**
   * // no tag
   */
  bar: SCISO8601Date;

  type: ThingType.InferredTypeFilterable;
}

export const testConfig: MapAggTestOptions = {
  testName: 'filterable tags should not override inferred tags',
  name: ThingType.InferredTypeFilterable,
  map: {
    maps: {
      foo: {
        type: 'date',
        fields: {
          raw: {
            type: 'keyword',
          },
        },
      },
      bar: {
        type: 'date',
      },
    },
  },
};
