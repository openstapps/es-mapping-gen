/*
 * Copyright (C) 2020 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ThingType} from './types';
import {MapAggTestOptions} from '../../map-agg-test-options';

/**
 * @date
 */
export type SCISO8601Date = string;

/**
 * @indexable
 */
export interface DateAndRange {
  /**
   * @date
   */
  directDate: string;

  dateAlias: SCISO8601Date;

  type: ThingType.Date;
}

export const testConfig: MapAggTestOptions = {
  testName: 'Dates and date ranges should have the correct type',
  name: ThingType.Date,
  map: {
    maps: {
      directDate: {
        type: 'date',
      },
      dateAlias: {
        type: 'date',
      },
    },
  },
};
