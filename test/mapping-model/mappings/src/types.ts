/*
 * Copyright (C) 2020-2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

export enum ThingType {
  MapExplicitTypes = 'map explicit types',
  DoubleTypeConflict = 'double type conflict',
  IncompatibleType = 'incompatible type',
  SensibleDefaultType = 'sensible default',
  InvalidTag = 'invalid tag',
  MissingPremap = 'missing premap',
  DefaultGeneric = 'default generic',
  Generics = 'generics',
  Nested = 'nested',
  IndexSignature = 'index signature',
  ImpossibleUnion = 'impossible union',
  TypeQuery = 'type query',
  ObjectUnion = 'object union',
  TypeWrapperInheritance = 'type wrapper inheritance',
  SortableTag = 'sortable tag',
  Enum = 'enum',
  InheritedProperty = 'inherited property',
  PairedTags = 'paired tags',
  FilterableTag = 'filterable tag',
  AnyUnknown = 'any unknown',
  InferredTypeFilterable = 'inferred type filterable',
  Date = 'date',
  InheritTags = 'inherit tags',
  TagsIgnoreCase = 'tags ignore case',
  TypeAlias = 'type alias',
  TypeOverrides = 'type overrides',
}
