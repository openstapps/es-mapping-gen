/*
 * Copyright (C) 2020-2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Logger} from '@openstapps/logger';
import {readdirSync, statSync} from 'fs';
import path from 'path';
import {MapAggTest} from './mapping-model/map-agg-test';
import {MapAggTestOptions} from './mapping-model/map-agg-test-options';

describe('ES Mapping Gen', async () => {
  const magAppInstance = new MapAggTest('mappings');

  /**
   * Expand a path to a list of all files deeply contained in it
   */
  // eslint-disable-next-line unicorn/consistent-function-scoping
  function expandPathToFilesSync(sourcePath: string, accept: (fileName: string) => boolean): string[] {
    const fullPath = path.resolve(sourcePath);
    const directory = statSync(fullPath);

    return directory.isDirectory()
      ? // eslint-disable-next-line unicorn/prefer-spread
        ([] as string[]).concat(
          ...readdirSync(fullPath).map(fragment =>
            expandPathToFilesSync(path.resolve(sourcePath, fragment), accept),
          ),
        )
      : [fullPath].filter(accept);
  }

  for (const file of expandPathToFilesSync('./test/mapping-model/mappings/', file =>
    file.endsWith('mapping-test.ts'),
  )) {
    try {
      // eslint-disable-next-line unicorn/no-await-expression-member
      const test = (await import(file))['testConfig'] as MapAggTestOptions;

      it(test.testName, function () {
        magAppInstance.testInterfaceAgainstPath(test);
      });
    } catch (error) {
      await Logger.error('UNHANDLED REJECTION', error.stack);
      process.exit(1);
    }
  }
})
  .timeout(20_000)
  .slow(10_000);
